# DEXCS-CHT-MR

CHTマルチリージョン計算用のDEXCS方式テンプレートケースファイル

## 背景

- OpenFOAMのCHTマルチリージョンケースの新規作成は、標準チュートリアルケースを雛形として改変する方法では初･中級者にとって到底困難である。
- TreeFoamのMultiRegion操作ツールを使って、汎用化された雛形ファイルを作成。これを出発点として任意のマルチリージョンメッシュを対象にGUIで簡単にケースセットアップ、計算実行出来る仕組みを使うのがDEXCSでの推奨方式。
- しかし、SHM（snappyHexMesh）によるマルチリージョンメッシュは比較的容易に作成可能であるが、連成境界へのレイヤー付加が別作業として必要になる点と、複雑形状に対するレイヤー作成において問題が多い。
- cfMeshはSHMと比べてレイヤー作成面での問題は少ないが、マルチリージョンメッシュは作成出来ない。

## 目的（着眼点）

TreeFoamマニュアルに記載の方法で作成したCHTケースファイルをテンプレートとして、個別リージョン毎に、メッシュをcfMeshで作成した個別の本番メッシュで置き換え、簡単なGUIセットアップで、cfMeshで作成したメッシュを使ったマルチリージョンケースが作成できると考えられたので、そのテンプレートケースファイルと使用法を公開する事とした。

## 収録フォルダ（ファイル）とその使用方法・内容説明

### multiRegeonTemplate

- TreeFoamマニュアルに記載の方法で作成したCHTケースファイル
- これをTreeFoamでそのまま実行すれば、気流中で直立した円柱の底面を加熱された状況での円柱表面からの放熱状況を計算できる
- これをTreeFoamでcaseコピーして、別の名前に変更して使用する。
- 流体、個体の領域ごと、cfMeshで個別に作成したメッシュを用意してメッシュを入れ替える。
- ケースセットアップの方法は、 解説資料（howToCHTCaseByCfMesh）を参照されたい。

### multiRegeonSimpleTemplate
- 同上で、定常計算（chtMultiRegionSimpleFoam）用に作り直したもの。

### cfMesh/cylinder
- SHMで作成したのと同じ形状モデルを使って、領域（fluid/solid）毎のcfMesh作成用のケースファイル一式で、メッシュも収納されている。

### cfMesh/heatSink
- DEXCSチュートリアル（cfMeshLesson）中に収納されたheatSinkモデルを使って領域（fluid/solid）毎のcfMesh作成用のケースファイル一式で、メッシュも収納されている。
- 但し、solid モデルにおいて、DEXCSチュートリアルではすべての面を同一の名前としていたが、ここでは発熱面と非連成境界面を区分している。
- 同梱のメッシュファイル（constant/polyMesh）は、DEXCSマクロで作成した後、スケール変更（mm->m, 1/1000）してある。メッシュを作り直す場合には、この手順も併せて実施する事。

### howToCHTCaseByCfMesh.pdf
- 解説資料（ハンズオン形式）

以下は、直接使用する事は無いが、参考用に収録しておいた。

### SHM
- SHM作成用のケースファイル。直方体中に直立円柱のあるモデルにて、TreeFoamマニュアルに則って作成。レイヤー作成は未実施。
- 使用したSTLファイル及びそれらを作成したFreeCADモデルも同梱してある。
- 寸法はTreeFoamマニュアルに記載のものと同一であるが、座標原点が少し異なるので、SHM作成時のmeshPoint座標に注意。

### multiRegionAirMaster
- TreeFoamマニュアルに記載の方法で作成したmasterCase
- ベースとした標準チュートリアルは　heatTransfer/chtMultiRegionFoam/multiRegionHeater

### multiRegionAirSteadyMaster
- 同上で、ベースとした標準チュートリアルは　heatTransfer/chtMultiRegionSimpleFoam/multiRegionHeaterRadiation
- 但し、ここで放射計算はしないので、radiationPropertiesはmultiRegionAirMasterのものに変更し、放射計算用のフィールド変数も収納していない。

## 動作環境

DEXCS2020 / OpenFOAM-v2006


